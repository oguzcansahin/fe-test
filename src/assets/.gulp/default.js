'use strict';

var gulp = require('gulp');


var defaultGulp = [
  'main:html',
  'main:static',
  'main:styles',
  'main:scripts',
  'vendor:bootstrap',
  'vendor:jquery',
  'vendor:slickjs',
  'vendor:svgxuse'
];

gulp.task('default', defaultGulp);
