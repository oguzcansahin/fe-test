'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');

var sourcemaps = require('gulp-sourcemaps');

var minify = require('gulp-clean-css');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');

var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

var rename = require('gulp-rename');

var browserSync = require('browser-sync');

var onError = function(err) {
  console.log(err.toString());
  this.emit('end');
};

gulp.task('vendor:slickjs:styles', function() {

  return gulp.src('src/assets/vendor/slick-carousel/slick/slick.scss')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'expanded',
      includePaths: ['src/assets/vendor/slick-sass/']
    }))
    .pipe(rename('slick.css'))
    .pipe(autoprefixer('last 2 version', 'iOS 8'))
    .pipe(gulp.dest('dist/assets/css'))
    .pipe(minify({
      keepSpecialComments: 0
    }))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/assets/css'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('vendor:slickjs:scripts', function() {

  return gulp.src('src/assets/vendor/slick-carousel/slick/slick.min.js')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(gulp.dest('dist/assets/js'))
    .pipe(concat('slick.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/assets/js'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('vendor:slickjs', ['vendor:slickjs:styles', 'vendor:slickjs:scripts']);
