/**
 * Pixel2HTML - 1/FeTest
 */

var msg = 'Pixel2HTML - 1/FeTest';

function printLog(log){
    'use strict';
  return console && console.log(log);
}

printLog(msg);

(function($){
  'use strict';
  $('.content-slider').slick({
    fade: true,
    mobileFirst: true,
    dots: true,
    dotsClass: 'slick-dots col-xs-12 col-md-push-6 col-md-4',
    nextArrow: '<div class="slick-button-wrap"><button type="button" class="slick-next"><svg class="icon icon--arrow__right" aria-hidden="true" role="img" width="17" width="32" version="1.1" viewBox="0 0 17 32"><use xlink:href="assets/icons/symbol-defs.svg#icon--arrow__right"></use></svg></button></div>',
    appendArrows: '.slick-list',
    appendDots: '.slick-list'
  });
})(jQuery);
